<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8" />
    <title>MaKearTing</title>
    <?php include 'util/links.php';?>
</head>

<body>

    <?php include 'components/nav.php';?>
    <?php include 'modules/main/main.php';?>
    <?php include 'components/footer.php';?>

    <?php include 'util/scripts.php';?>

</body>

</html>