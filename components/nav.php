<?php ?>
<!-- Navigation Start-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/modules/main/main.php">
                <img src="img/logo.png" width="200" height="50" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Dashboard
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Atrair
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Postagens em Mídias Sociais</a>
                            <a class="dropdown-item" href="#">Anúncios no Facebook</a>
                            <a class="dropdown-item" href="#">Painel de palavras-chaves</a>
                            <a class="dropdown-item" href="#">Otimização de Páginas (SEO)</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Converter
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Landing Pages</a>
                            <a class="dropdown-item" href="#">Formulários</a>
                            <a class="dropdown-item" href="#">Pop-ups</a>
                            <a class="dropdown-item" href="#">Campos personalizados</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Relacionar
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Base de Leads</a>
                            <a class="dropdown-item" href="#">Lead Scoring</a>
                            <a class="dropdown-item" href="#">Lead Tracking</a>
                            <a class="dropdown-item" href="#">Segmentação de Leads</a>
                            <a class="dropdown-item" href="#">Automação de Marketing</a>
                            <a class="dropdown-item" href="#">Email</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Analisar
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Marketing BI</a>
                            <a class="dropdown-item" href="#">Análise de Canais</a>
                            <a class="dropdown-item" href="#">Alcance</a>
                            <a class="dropdown-item" href="#">Páginas mais acessadas</a>
                            <a class="dropdown-item" href="#">Relatórios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Your name
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Conta</a>
                            <a class="dropdown-item" href="#">Planos de Sucesso</a>
                            <a class="dropdown-item" href="#">Minha Conta</a>
                            <a class="dropdown-item" href="#">Sair</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Navigation End-->
