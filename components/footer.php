<!-- Footer -->
<footer class="page-footer font-small indigo">

    <!-- Footer Links -->
    <div class="container">

      <!-- Grid row-->
      <div class="row text-center d-flex justify-content-center pt-5 mb-3">

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a class="link-footer" href="#!">Sobre nós</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a class="link-footer" href="#!">Planos</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a class="link-footer" href="index.php">Dashboard</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a class="link-footer" href="#!">FAQ</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a class="link-footer" href="#!">Contato</a>
          </h6>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->
      <hr class="rgba-white-light" style="margin: 0 15%;">

      <!-- Grid row-->
      <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

        <!-- Grid column -->
        <div class="col-md-8 col-12 mt-5">
          <p style="line-height: 1.7rem">A MaKearTing é um projeto de conclusão de curso desenvolvida para baratear e também facilitar a criação do Marketing para as empresas, focada principalmente em Supermercados.</p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->
      <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">
  <center>
      <!-- Grid row-->
      <div class="row pb-3">

        <!-- Grid column -->
        <div class="col-md-12">

          <div class="mb-5 flex-center">

            <!-- Facebook -->
            <a class="fb-ic" href="https://www.facebook.com/dblinks.agenciadigital" target="_blank">
              <i class="fa fa-facebook fa-lg white-text mr-4"> </i>
            </a>
            <!-- Twitter -->
            <a class="tw-ic" href="https://www.twitter/dblinksagenciadgital target="_blank">
              <i class="fa fa-twitter fa-lg white-text mr-4"> </i>
            </a>
            <!--Linkedin -->
            <a class="li-ic" href="https://www.linkedin.com/company/dblinks-ag%C3%AAncia-digital/" target="_blank">
              <i class="fa fa-linkedin fa-lg white-text mr-4"> </i>
            </a>
            <!--Instagram-->
            <a class="ins-ic" href="https://www.instagram.com/dblinksagencia/" target="_blank">
              <i class="fa fa-instagram fa-lg white-text mr-4"> </i>
            </a>

          </div>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
    <!-- Footer Links -->
    </center>
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright: 
      <a class="link-footer" href="https://www.dblinks.com.br" target="_blank">   DB'links</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->